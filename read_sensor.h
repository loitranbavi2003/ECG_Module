/**
 ******************************************************************************
 * @file    read_sensor.h
 * @author  DonNV
 * @version V1.0
 * @date
 * @brief   This file contains all the functions prototypes for the read data from sensor
 *          library.
 ******************************************************************************
 */
#ifndef READ_SENSOR_H
#define READ_SENSOR_H

#include <unistd.h>  //Used for UART
#include <fcntl.h>   //Used for UART
#include <termios.h> //Used for UART
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <chrono>
#include <QtMultimedia/QMediaPlayer>
#define PKG_DATA_LEN 12
#define BUFFER_SIZE 300

class read_sensor
{

public:
    int receiver_data_from_sensor(int *uart0_filestream);
    unsigned char calculator_sum(unsigned char *pkg, int len);
    void parse_package(unsigned char *pkgdata, unsigned int pkglength);
    int receiver_data_queue(int *uart0_filestream);

    unsigned char spo2_data();
    unsigned char pr_data();
    unsigned char status_connect_spo2();
    unsigned char spo2_wave();
    unsigned char spo2_pic();

    unsigned char cuff_data();
    unsigned char sys_data();
    unsigned char mean_data();
    unsigned char dia_data();
    unsigned char status_nibp_process();

    double temperature_data();
    unsigned char status_connect_temperature();

    unsigned char ecg_wave();
    unsigned char ecg_leadII_wave();
    unsigned char ecg_leadIII_wave();
    unsigned char ecg_leadaVR_wave();
    unsigned char ecg_leadaVL_wave();
    unsigned char ecg_leadaVF_wave();
    unsigned char ecg_leadV_wave();

    unsigned char status_ecg_process();
    unsigned char ecg_heart_data();
    unsigned char ecg_resp_rate_data();
    unsigned char hr_ecg_pic();

    unsigned char resp_wave();

private:
    enum PKG_TYPE
    {
        PKG_ECG_WAVE = 0x01,
        PKG_ECG_PARAM = 0x02,
        PKG_NIBP = 0x03,
        PKG_SPO2 = 0x04,
        PKG_TEMP = 0x05,
        PKG_TXY = 0x06,
        PKG_FGN = 0x07,
        PKG_FGN_RATIO = 0x0e,
        PKG_HR_ECG_PEAK = 0x30,
        PKG_SPO2_PEAK = 0x31,
        PKG_SW_VER = 0xfc,
        PKG_HW_VER = 0xfd,
        PKG_SPO2_WAVE = 0xfe,
        PKG_RESP_WAVE = 0xff
    };

    enum SPO2_STATUS
    {
        SPO2_NORMAL = 0x00,
        SPO2_SENSOR_OFF = 0x01,
        SPO2_NO_FINGER_INSERT = 0x02,
        SPO2_SEARCH_PULSE_SIGNAL = 0x03,
        SPO2_SEARCH_TIME_OUT = 0x04
    };

    enum TEMP_STATUS
    {
        TEMP_NORMAL = 0x00,
        TEMP_SENSOR_OFF = 0x01
    };

    unsigned char spo2 = 0, pr = 0, status_spo2 = 0, spo2_wave_point = 0;
    unsigned char cuff = 0, sys = 0, mean = 0, dia = 0, status_nibp = 0;
    unsigned char status_temperature = 0, temperature_inte, temperature_deci;
    double temperature = 0;
    unsigned char ecg_wave_point = 0, ecg_status = 0, ecg_heart_rate = 0, ecg_resp_rate = 0;
    unsigned char ecg_lead2_wave_point = 0, ecg_lead3_wave_point = 0;
    unsigned char ecg_leadaVR_wave_point = 0, ecg_leadaVL_wave_point = 0;
    unsigned char ecg_leadaVF_wave_point = 0, ecg_leadV_wave_point = 0;
    unsigned char resp_wave_point = 0;
    unsigned char pic_spo2_status = 0;
    unsigned char pic_hr_status = 0;

    unsigned char count_time = 0;
};

#endif
// EOF
