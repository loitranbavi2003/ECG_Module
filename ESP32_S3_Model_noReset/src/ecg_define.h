#ifndef _ECG_DEFINE_H_
#define _ECG_DEFINE_H_

#include <stdint.h>
#include <Arduino.h>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <HardwareSerial.h>
#include <Wire.h>

#include "ecg_fsm.h"
#include "ecg_message.h" 
#include "ecg_convert.h"
#include "ringbuffer.h"

#define LEN_FRAME_ROTATION 100

#define DEBUG_PRINT_FEATURE 1

void ECG_Init(void);
void ECG_Run(void);
void TIMER_Init(void);

#endif 
