/**
 * @file ringbuffer.h
 * @author longht (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2024-04-18
 *
 * @copyright Copyright (c) 2024
 *
 */

#ifndef RING_BUFFER_H_
#define RING_BUFFER_H_

#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>

#define NUMS_IN_WINDOW 200

/**
 * Class which holds a ring buffer.
 * The buffer contains a buffer array
 * as well as metadata for the ring buffer.
 */
template <typename T>
class RING_BUFFER
{
private:
  /** Buffer memory. */
  T *buffer;
  /** Buffer capacity */
  int capacity;
  /** Buffer mask. */
  int buffer_mask;
  /** Index of tail. */
  int tail_index;
  /** Index of head. */
  int head_index;

  /** Only for sound samples buffer */
  bool window_full = false;

  uint32_t counter = 0;

  /** The beginning index of a sub-array */
  int beginning_index = 0;
  /** The ending index of a sub-array */
  int ending_index = 0;

public:
  /**
   * @brief Construct a new ring buffer object
   *
   */
  RING_BUFFER(){};

  /**
   * @brief Destroy the ring buffer object
   *
   */
  ~RING_BUFFER(){};

  /**
   * @brief Construct a new ring buffer object
   *
   * @param buf
   * @param buf_size
   */
  RING_BUFFER(T *buf, int buf_size);

  /**
   * @brief Initializes the ring buffer pointed to by buffer
   * This function can also be used to empty/reset the buffer.
   * The resulting buffer can contain buf_size-1 <T>.
   * @param buf The buffer allocated for the ring buffer.
   * @param buf_size The size of the allocated ring buffer.
   */
  void Init(T *buf, int buf_size);

  /**
   * Adds a <T>data to a ring buffer.
   * @param data The data to push.
   */
  void Enqueue(T data);

  /**
   * Adds an array of <T>data to a ring buffer.
   * @param data A pointer to the array of <T> to Put in the queue.
   * @param size The size of the array.
   */
  void Enqueue_Array(T *data, int len);

  /**
   * Returns the oldest <T>data in a ring buffer.
   * @param data A pointer to the location at which the data should be Put.
   * @return 1 if data was returned; 0 otherwise.
   */
  int Dequeue(T *data);

  /**
   * Returns the <em>len</em> oldest <T>data in a ring buffer.
   * @param data A pointer to the array at which the data should be Put.
   * @param len The maximum number of data to return.
   * @return -1 if required length is invalid; 0 if buffer is empty; otherwise The number of <T>data returned.
   */
  int Dequeue_Array(T *data, int len);

  /**
   * Peeks a ring buffer, i.e. returns an element without removing it.
   * @param data A pointer to the location at which the data should be gotten.
   * @param index The index to peek.
   * @return 1 if data was returned; 0 otherwise.
   */
  int Peek(T *data, int index);

  /**
   * Returns the <em>len</em> oldest <T>data in a ring buffer without changing head and tail index.
   * @param data A pointer to the array at which the data should be Put.
   * @param len The maximum number of data to return.
   * @return -1 if required length is invalid; 0 if buffer is empty; otherwise The number of <T>data returned.
   */
  int Get_Array(T *data, int len);

  /**
   * Returns whether a ring buffer is empty.
   * @param buffer The buffer for which it should be returned whether it is empty.
   * @return 1 if empty; 0 otherwise.
   */
  int Is_Empty()
  {
    return (this->counter == 0) ? 1 : 0;
  }

  /**
   * Returns whether a ring buffer is full.
   * @param buffer The buffer for which it should be returned whether it is full.
   * @return 1 if full; 0 otherwise.
   */
  int Is_Full()
  {
    return (this->counter == this->capacity) ? 1 : 0;
  }

  /**
   * Returns the number of items in a ring buffer.
   * @param buffer The buffer for which the number of items should be returned.
   * @return The number of items in the ring buffer.
   */
  int Count()
  {
    return this->counter;
  }

  /**
   * @brief Get the tail index
   *
   * @return int
   */
  int Get_Tail()
  {
    return this->tail_index;
  }

  /**
   * @brief Set the tail index
   *
   * @param index
   */
  void Set_Tail(int index)
  {
    this->tail_index = index;
  }

  /**
   * @brief Get the capacity of the buffer
   * 
   * @return int 
   */
  int Get_Capacity()
  {
    return this->capacity;
  }

  /**
   * @brief Get the head index
   *
   * @return int
   */
  int Get_Head()
  {
    return this->head_index;
  }

  /**
   * @brief Get the buffer mask
   *
   * @return int
   */
  int Get_Buffer_Mask()
  {
    return this->buffer_mask;
  }

  /**
   * @brief Set the beginning index to extract array from buffer
   *
   * @param index
   */
  void Set_Beginning_Index(int index);

  /**
   * @brief Get the beginning index to extract array from buffer
   * 
   * @return int 
   */
  int Get_Beginning_Index();

  /**
   * @brief Return if buffer is fulfilled
   * 
   * @return true: if the window fulfilled by the num_window_state 
   * @return false: otherwise
   */
  bool Get_Window_State();

  /**
   * @brief Set the window state
   * 
   * @param state: true - if the window fulfilled by the num_window_state; false - otherwise
   */
  void Set_Window_State(bool state);

  /**
   * @brief Get the buffer mask
   * @param start: the start index
   * @param align: the align index
   * @return max_value: the maximum value in the buffer, max_index: the index of the maximum value
   */
  std::pair<T, int> MaxAndIndex(int start, int align);
};

#include "ringbuffer.tpp"

#endif // !RING_BUFFER_H_