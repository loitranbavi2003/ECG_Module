#include "ecg_define.h"

ecg_fsm fsm;
HardwareSerial uart1(1); // Use hardware UART1
hw_timer_t *My_timer = NULL;

/* result: chứa giá trị lớn nhất trong frame data và vị trí của giá trị lớn nhất */
std::pair<uint16_t, int> result;

static bool     flag_downsample = false;

static uint8_t  flag_model_ecg = 0;
static uint8_t  flag_frame_full = 0;
static uint8_t  flag_buffer_full = 0;

static uint16_t max_index       = 0;
static uint16_t data_ecg_rec    = 0;
static uint16_t start_index     = 0;
static uint16_t len_frame_data  = 0;

static uint16_t arr_data_ecg[170];
static uint16_t arr_frame_buffer[300];

uint16_t arr_buffer[4000];
RING_BUFFER<uint16_t> ring_buffer(arr_buffer, sizeof(arr_buffer) / sizeof(arr_buffer[0]));

void IRAM_ATTR onTimer()
{
  if(uart1.available()) 
  {
    if(flag_downsample)
    {
      uart1.read(); 
      flag_downsample = false;
    }
    else
    {
      data_ecg_rec = uart1.read();
      /* Đưa data vào buffer */
      ring_buffer.Enqueue(data_ecg_rec);
      flag_downsample = true;
    }
  }
}

void ECG_Init(void)
{
  /* Init serial */
  Serial.begin(115200);
  /* 18 - Rx | 17 - Tx */
  uart1.begin(115200, SERIAL_8N1, 18, 17);
  /* Config Timer */
  TIMER_Init();
}

void ECG_Run(void)
{
  if(ring_buffer.Count() >= LEN_FRAME_ROTATION)
  {
    /* Tìm giá trị lớn nhất và vị trí của giá trị lớn nhất */
    result = ring_buffer.MaxAndIndex(start_index, LEN_FRAME_ROTATION);
    /* Vị trí của giá trị lớn nhất */
    max_index = result.second;

    /* Không lấy trường hợp giá trị Max ở 50 ptu đầu tiên */
    if(max_index < 50)
    {
      start_index = 50;
    }
    else if((max_index + 50 < ring_buffer.Get_Capacity()) && max_index > start_index)
    {
      if(flag_buffer_full == 0)
      {
        flag_frame_full = 1;
        // Serial.println("Frame buffer full");
      }
    }
    /* max_index vượt hết 1 vòng buffer */
    else if((max_index + 50 < ring_buffer.Get_Capacity()) && max_index < start_index)
    {
      if(flag_frame_full == 0)
      {
        flag_buffer_full = 1;
        Serial.println("Max index before start buffer");
      }
    }
    else if((max_index + 50 > ring_buffer.Get_Capacity()) && max_index > start_index)
    {
      if(flag_frame_full == 0)
      {
        flag_buffer_full = 1;
        Serial.println("Max index after start buffer");
      }
    }

    if(flag_frame_full == 1 && (ring_buffer.Get_Head() >= max_index + 50))
    {
      Serial.println("Frame buffer full");
      Serial.print("Count: ");
      Serial.println(ring_buffer.Count());
      Serial.print("Get_Head: ");
      Serial.println(ring_buffer.Get_Head());
      Serial.print("max_index: ");
      Serial.println(max_index);

      len_frame_data = max_index + 50 - start_index;
      ring_buffer.Dequeue_Array(arr_frame_buffer, len_frame_data);
      start_index = max_index + 50;
      ring_buffer.Set_Tail(start_index);

      /* Copy data from arr_frame_buffer to arr_data_ecg */
      if(len_frame_data > 100)
      {
        for(int i = len_frame_data - 100; i < len_frame_data; i++)
        {
          arr_data_ecg[i - (len_frame_data - 100)] = arr_frame_buffer[i];
        }

        for(int i = 100; i < 170; i++)
        {
          arr_data_ecg[i] = 0;
        }
      }
      else
      {
        for(int i = 0; i < len_frame_data; i++)
        {
          arr_data_ecg[i] = arr_frame_buffer[i];
        }

        for(int i = len_frame_data; i < 170; i++)
        {
          arr_data_ecg[i] = 0;
        }
      }

      flag_model_ecg = 1;
      flag_frame_full = 0;
    }

    if(flag_buffer_full == 1 && (ring_buffer.Get_Head() >= ((max_index + 50) % ring_buffer.Get_Capacity())))
    {
      Serial.print("Count: ");
      Serial.println(ring_buffer.Count());
      Serial.print("Get_Head: ");
      Serial.println(ring_buffer.Get_Head());
      Serial.print("max_index: ");
      Serial.println(max_index);
      
      uint16_t len_frame_data_1 = ring_buffer.Get_Capacity() - start_index;
      uint16_t len_frame_data_2 = (max_index + 50) % ring_buffer.Get_Capacity();
      len_frame_data = len_frame_data_1 + len_frame_data_2;
      ring_buffer.Dequeue_Array(arr_frame_buffer, len_frame_data);
      start_index = (max_index + 50) % ring_buffer.Get_Capacity();
      ring_buffer.Set_Tail(start_index);

      /* Copy data from arr_frame_buffer to arr_data_ecg */
      if(len_frame_data > 100)
      {
        for(int i = len_frame_data - 100; i < len_frame_data; i++)
        {
          arr_data_ecg[i - (len_frame_data - 100)] = arr_frame_buffer[i];
        }

        for(int i = 100; i < 170; i++)
        {
          arr_data_ecg[i] = 0;
        }
      }
      else
      {
        for(int i = 0; i < len_frame_data; i++)
        {
          arr_data_ecg[i] = arr_frame_buffer[i];
        }

        for(int i = len_frame_data; i < 170; i++)
        {
          arr_data_ecg[i] = 0;
        }
      }

      flag_model_ecg = 1;
      flag_buffer_full = 0;
    }

    if(flag_model_ecg == 1)
    {
      /* Run model */
      flag_model_ecg = 0;
    }
  }
}

void TIMER_Init(void)
{
  My_timer = timerBegin(0, 80, true);
  timerAttachInterrupt(My_timer, &onTimer, true);
  timerAlarmWrite(My_timer, 1000, true);
  timerAlarmEnable(My_timer);
}
