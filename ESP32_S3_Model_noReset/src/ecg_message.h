#ifndef _ECG_MESSAGE_H_
#define _ECG_MESSAGE_H_

#include "ecg_define.h"

#define START_BYTE 0xAA55

class ecg_message
{
    private:

    public:
        /*
            @brief: Khai báo kiểu dữ liệu Frame.
            @node: Chức năng của kiểu dữ liệu này là chứa thông tin của Frame.
            @param StartFrame: Là một biến kiểu uint16_t chứa thông tin Start Frame.
            @param LengthData: LengthData = 1 byte (LengthData) + n byte (Data) + 1 byte (CheckFrame).
        */
        typedef struct
        {
            uint16_t    StartFrame;
            uint8_t     LengthData;
            uint8_t     Data[96];
            uint8_t     CheckFrame;
        }ecg_FrameMsg_t;
        
        /*
            @brief: Hàm tạo Frame data chuẩn.
            @node: Chức năng chính của hàm để tạo các bản tin AskType, AswerType, AskData, AnswerData
            @param datain: Là một struct chưa các thông tin được người dùng chỉ định để tạo bản tin.
            @param dataout: Là con trỏ dùng để lấy mảng sao khi tạo thành công.
            @retval: Trả về 0 là lỗi, Lớn hơn 0 là thành công.
        */
        uint8_t ECG_Message_Create_Frame(ecg_FrameMsg_t data_in, uint8_t *data_out);                                              

        /*
            @brief: Hàm tách Frame data.
            @node: Chức năng của hàm là chuyển data từ dạng arr về dạng struct.
            @param datain: Là một mảng chứa data.
            @param dataout: Là một struct chứa data đầu ra.
            @retval: trả về 1 thành công.
        */
        uint8_t ECG_Message_Detect_Frame(uint8_t *data_in, ecg_FrameMsg_t *data_out);
        uint8_t ECG_CheckSum(uint8_t *buf, uint8_t len);

        /*
            @brief: Hàm gửi data qua UART1.
            @node: Chức năng của hàm là gửi data qua UART1.
            @param data: Là một biến kiểu uint8_t chứa data.
            @retval: Không có.
        */
        void ECG_Send_DataToUart0(uint8_t data);
        
        /*
            @brief: Hàm in ra màn hình thông tin Frame gửi đi.
            @node: Chức năng của hàm là in ra màn hình thông tin Frame gửi đi.
            @param data: Là một mảng chứa data.
            @param len: Là chiều dài của data.
            @retval: Không có.
        */
        void ECG_Print_Frame_Send(uint8_t *data, uint8_t len);

        /*
            @brief: Hàm in ra màn hình thông tin Frame nhận được.
            @node: Chức năng của hàm là in ra màn hình thông tin Frame nhận được.
            @param data: Là một struct chứa data.
            @param len: Là chiều dài của data.
            @retval: Không có.
        */
        void ECG_Print_Frame_Rec(ecg_FrameMsg_t *data, uint8_t len);
};

#endif