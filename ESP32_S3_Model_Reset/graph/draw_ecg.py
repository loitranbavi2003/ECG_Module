import matplotlib.pyplot as plt

# Chuỗi chứa dữ liệu ECG
# str = "109 109 109 100 105 108 115 114 106 115 113 112 113 110 118 115 117 119 114 121 117 117 120 120 127 116 116 120 110 110 105 106 111 108 113 109 105 108 106 109 105 106 109 104 108 107 107 107 104 112 104 103 112 106 109 109 107 106 103 113 107 104 112 108 110 110 114 115 102 107 109 107 110 108 113 108 105 109 103 121 135 128 109 101 116 115 110 112 108 117 112 107 116 113 116 112 109 117 116 121 120 119 123 117 125 125 121 123 120 125 121 116 120 115 117 111 107 113 106 113 113 108 110 107 110 109 108 113 106 111 112 110 113 110 116 109 103 114 108 110 111 110 114 108 113 113 109 115 111 116 113 110 117 105 108 113 112 113 109 112 111 108 113 112 131 139 116 106 106 121";
str = "122 123 119 126 117 118 122 122 129 124 127 130 124 129 129 132 131 127 134 125 127 129 122 123 121 121 120 113 118 115 119 120 111 117 116 117 117 113 121 118 116 117 114 119 117 119 119 115 121 115 116 120 118 120 113 117 122 118 122 121 118 117 114 125 119 117 121 118 123 121 119 120 118 124 113 112 120 117 126 136 140 126 109 119 123 123 124 121 128 119 119 126 122 126 126 126 129 126 131 129 128 132 131 135 128 128 135 131 136 133 129 132 126 130 123 117 124 121 123 120 118 123 119 121 119 119 122 119 124 119 118 126 121 122 122 122 125 118 124 123 120 124 114 119 123 123 124 119 126 122 118 124 120 125 122 118 126 125 126 119 117 124 119 125 124 118 121 118 122 134";

# Chuyển đổi chuỗi sang mảng số nguyên
data = list(map(float, str.split()))

# Tạo một đồ thị
plt.figure()

# Vẽ đồ thị
plt.plot(data)

# Đặt tiêu đề cho đồ thị
plt.title('My Data')

# Hiển thị đồ thị
plt.show()

# ----------------------------------------------------------------------------
# Số lượng phần tử trong file
# with open('graph/1_comma.csv', 'r') as file:
#     data = file.read()
#     elements = data.split(",")
#     num_elements = len(elements)

# print(f'The file contains {num_elements} elements.')

# ----------------------------------------------------------------------------
# Đọc dữ liệu từ file và ghi vào file khác
# Mở file và đọc nội dung
# with open('graph/1.csv', 'r') as file:
#     data = file.read()

# # Thay thế khoảng trắng bằng dấu phẩy
# data = data.replace(' ', ',')

# # Ghi nội dung đã thay đổi vào file mới
# with open('graph/1_comma.csv', 'w') as file:
#     file.write(data)