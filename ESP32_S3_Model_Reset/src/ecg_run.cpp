#include "ecg_define.h"

ecg_fsm fsm;
HardwareSerial uart1(1); // Use hardware UART1
hw_timer_t *My_timer = NULL;

/* result: chứa giá trị lớn nhất trong frame data và vị trí của giá trị lớn nhất */
std::pair<uint16_t, int> result;

static bool     flag_downsample   = false;

static uint16_t max_index         = 0;
static uint16_t start_index       = 0;
static uint16_t data_ecg_rec      = 0;
static uint16_t len_frame_data    = 0;
static uint16_t start_frame_index = 0;

static uint16_t arr_data_ecg[170];
static uint16_t arr_frame_buffer[300];

uint16_t arr_buffer[3000];
RING_BUFFER<uint16_t> ring_buffer(arr_buffer, sizeof(arr_buffer) / sizeof(arr_buffer[0])); 

void IRAM_ATTR onTimer()
{
  if (!ring_buffer.Is_Full())
  {
    if(uart1.available()) 
    {
      if(flag_downsample)
      {
        uart1.read(); 
        flag_downsample = false;
      }
      else
      {
        data_ecg_rec = uart1.read();
        /* Đưa data vào buffer */
        ring_buffer.Enqueue(data_ecg_rec);
        flag_downsample = true;
      }
    }
  }
}

void ECG_Init(void)
{
  /* Init serial */
  Serial.begin(115200);
  /* 18 - Rx | 17 - Tx */
  uart1.begin(115200, SERIAL_8N1, 18, 17);
  /* Config Timer */
  TIMER_Init();
}

void ECG_Run(void)
{
  if(ring_buffer.Count() >= LEN_FRAME_ROTATION)
  {
    /* Tìm giá trị lớn nhất và vị trí của giá trị lớn nhất */
    result = ring_buffer.MaxAndIndex(start_index, LEN_FRAME_ROTATION);
    /* Vị trí của giá trị lớn nhất */
    max_index = result.second;

    if(max_index + 90 >= ring_buffer.Get_Capacity() || max_index < start_index)
    {
      max_index = 0;
      start_index = 0;
      ring_buffer.Reset_RingBuffer();
      Serial.println("=================================== Reset Ring Buffer ====================================");
    }
    else if(ring_buffer.Get_Head() >= ((max_index + 90) % ring_buffer.Get_Capacity()))
    {
      #if DEBUG_PRINT
      Serial.print("Count: ");
      Serial.println(ring_buffer.Count());
      Serial.print("Get_Head: ");
      Serial.println(ring_buffer.Get_Head());
      Serial.print("max_index: ");
      Serial.println(max_index);
      #endif

      len_frame_data = max_index + 90 - start_index;
      ring_buffer.Get_Array(arr_frame_buffer, len_frame_data);
      start_index = (max_index + 90) % ring_buffer.Get_Capacity();
      ring_buffer.Set_Beginning_Index(start_index);

      Serial.print("len_frame_data: ");
      Serial.println(len_frame_data);

      if(len_frame_data > 170)
      {
        start_frame_index = len_frame_data - 170;
      }
      else
      {
        start_frame_index = 0;
      }
      for(uint16_t i = start_frame_index; i < len_frame_data; i++)
      {
        Serial.print(arr_frame_buffer[i]);
        Serial.print(" ");
      }
      Serial.println();
      Serial.println();
    }
  }
}

void TIMER_Init(void)
{
  My_timer = timerBegin(0, 80, true);
  timerAttachInterrupt(My_timer, &onTimer, true);
  timerAlarmWrite(My_timer, 1000, true);
  timerAlarmEnable(My_timer);
}
