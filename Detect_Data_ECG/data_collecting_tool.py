import serial
import csv
import time
 
SER_PORT = "COM4"
SER_BAUDRATE = 115200

timeout_seconds = 10

number = 5

ser = serial.Serial(
    # Serial Port to read the data from
    port=SER_PORT,
    # Rate at which the information is shared to the communication channel
    baudrate=SER_BAUDRATE,
    # Applying Parity Checking (none in this case)
    parity=serial.PARITY_NONE,
    # Pattern of Bits to be read
    stopbits=serial.STOPBITS_ONE,
    # Total number of bits to be read
    bytesize=serial.EIGHTBITS,
    # Number of serial commands to accept before timing out
    timeout=None,
)

time.sleep(1)
ser.flushInput()
ser.flushOutput()

while True:
    print("Enter Mode: ")
    print("1. Collect data")
    print("2. Exit")
    mode = int(input())
    if mode == 1:
        with open(
            "{}.csv".format(number),
            "w",
            newline="\n",
        ) as file:
            w = csv.writer(file)
            start_time = time.time()
            while True:
                line = ser.readline()
                if line:
                    written = line.decode("utf-8")
                    file.write(written)
                if time.time() - start_time > timeout_seconds:
                    ser.flushInput()
                    ser.flushOutput()
                    break

            number += 1
    elif mode == 2:
        print('Ending progcess.')
        quit()
