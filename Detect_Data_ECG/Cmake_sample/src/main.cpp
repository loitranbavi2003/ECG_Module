#include "ecg_define.h"

ecg_fsm fsm;

#define FILE_IN "../data/2.csv"
#define FILE_OUT "../data/data_ecg_2.csv"

static uint16_t data_rec = 0;
static uint16_t data_ecg_rec = 0;

int main()
{
    std::ifstream file_in(FILE_IN);
    std::ofstream file_out(FILE_OUT);

    if (!file_in.is_open())
    {
        std::cout << "Unable to open file_in" << std::endl;
        return 1;
    }

    fsm.ECG_ClearTimeOut_Fsm();

    std::string line;
    while (std::getline(file_in, line))
    {
        data_rec = std::stoi(line, nullptr, 16); // convert hex string to int
        // std::cout << data_rec << std::endl; // print the result
        if(fsm.ECG_RecMessage_Fsm(data_rec) == 1)
        {
            /* 0x01 - Bản tin của tín hiệu ECG */
            if (fsm_valueAll.data_out[3] == 0x01) 
            {
                /* Lấy phần tử đầu tiên trong bản tin của tín hiệu ECG */
                data_ecg_rec = fsm_valueAll.data_out[4];
                std::cout << "Data ECG: " << data_ecg_rec << std::endl;
                /* write the data to the output file */
                file_out << data_ecg_rec << ","; 
            }
        }
    }
    
    std::cout << "==================== Done! ====================" << std::endl;
    file_in.close();
    file_out.close();

    return 0;
}