#ifndef _ECG_DEFINE_H_
#define _ECG_DEFINE_H_

#include <stdint.h>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <iterator>

#include "ecg_fsm.h"
#include "ecg_message.h" 
#include "ecg_convert.h"

#endif 
