#include "sd_card.h"

void SDCARD_Init(void)
{
  if (!SD.begin(SD_CS)) {
    Serial.println("Card Mount Failed");
    return;
  }
  uint8_t cardType = SD.cardType();
  if (cardType == CARD_NONE) {
    Serial.println("No SD card attached");
    return;
  }
  Serial.println("------------------- SD Card attached -------------------");

  /*--------------- Write to file ---------------*/
#if WRITE_SDCARD
  File file = SD.open("/data.txt", FILE_WRITE);
  if (!file) {
    Serial.println("Failed to open file for writing");
    return;
  }

  if (file.print("Hello ESP32!")) {
    Serial.println("File written");
  } 
  else {
    Serial.println("Write failed");
  }
  file.close();
#endif

  /*--------------- Read from file ---------------*/
#if READ_SDCARD
  file = SD.open("/data.txt");
  if (!file) {
    Serial.println("Failed to open file for reading");
    return;
  }

  Serial.println("Read from file: ");
  while (file.available()) {
    Serial.write(file.read());
  }
  file.close();
#endif
}
