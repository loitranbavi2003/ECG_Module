#include "ecg_define.h"
/*------------------------------------------------------------------------------*/
#include "ecg_recognition6.h"

#include <tensorflow/lite/version.h>
#include "tensorflow/lite/micro/all_ops_resolver.h"
#include "tensorflow/lite/micro/micro_error_reporter.h"
#include "tensorflow/lite/micro/micro_interpreter.h"
#include "tensorflow/lite/schema/schema_generated.h"
#include <tensorflow/lite/schema/schema_generated.h>

const tflite::Model* tflModel = nullptr;
tflite::MicroInterpreter* tflInterpreter = nullptr;
TfLiteTensor* tflInput1Tensor = nullptr;
TfLiteTensor* tflInput2Tensor = nullptr;
TfLiteTensor* tflOutputTensor = nullptr;
constexpr int tensorArenaSize = 32 * 1024;
alignas(16) byte tensorArena[tensorArenaSize];
/*------------------------------------------------------------------------------*/

ecg_fsm fsm;
HardwareSerial uart1(1); // Use hardware UART1
hw_timer_t *My_timer = NULL;

StaticJsonDocument<256> doc;

/* result: chứa giá trị lớn nhất trong frame data và vị trí của giá trị lớn nhất */
std::pair<uint16_t, int> result;

static bool     flag_start        = false;
static bool     flag_downsample   = false;

static uint8_t  index_Q           = 0;
static uint8_t  index_R           = 0;
static uint8_t  index_S           = 0;
static uint16_t var_Q             = 0;
static uint16_t var_R             = 0;
static uint16_t var_S             = 0;

static uint16_t count_N           = 0;
static uint16_t count_S           = 0;
static uint16_t count_V           = 0;
static uint16_t count_F           = 0;
static uint16_t count_Q           = 0;

static std::string filename_S     = "";
static std::string filename_V     = "";
static std::string filename_F     = "";
static std::string filename_Q     = "";
static uint16_t count_filename_S  = 0;
static uint16_t count_filename_V  = 0;
static uint16_t count_filename_F  = 0;
static uint16_t count_filename_Q  = 0;

static uint8_t  flag_start_button = 0;
static uint8_t  var_ButtonReset   = 0;
static uint8_t  var_ButtonStop    = 0;

static uint16_t start_index       = 0;
static uint16_t data_ecg_rec      = 0;
static uint16_t len_frame_data    = 0;
static uint16_t max_index_next    = 0;
static uint16_t max_index_start   = 0;

static uint16_t count_time_send_msg = 0;

static uint32_t time_end_ECG_ModelRun = 0;
static uint32_t time_end_ECG_Derivative = 0;
static uint32_t time_end_ECG_SDCardWrite = 0;
static uint32_t time_start_ECG_ModelRun = 0;
static uint32_t time_start_ECG_Derivative = 0;
static uint32_t time_start_ECG_SDCardWrite = 0;

static uint16_t arr_QRS[6] = {0}; /* index - value R Q S */
static uint16_t arr_frame_buffer[170] = {0};
static uint16_t arr_data_ecg[LEN_ARR_DATA_ECG] = {0};
static int      arr_derivative[LEN_ARR_DATA_ECG] = {0};

uint16_t arr_buffer[4000];
RING_BUFFER<uint16_t> ring_buffer(arr_buffer, sizeof(arr_buffer) / sizeof(arr_buffer[0])); 

void IRAM_ATTR onTimer()
{
  BUTTON_Readall();

  // Wait 1.5s to send message JSON
  if(count_time_send_msg > 1500)
  {
    count_time_send_msg = 0;
  }
  else
  {
    count_time_send_msg++;
  }

  /* Save data to buffer */
  while(uart1.available())
  {
    if(!ring_buffer.Is_Full()) 
    {
      data_ecg_rec = uart1.read();
      if(fsm.ECG_RecMessage_Fsm(data_ecg_rec) == 1)
      {
        if(fsm_valueAll.data_out[ECG_WAVES_INDEX] == ECG_WAVES)
        {
          if(flag_downsample == true)
          {
            flag_downsample = false;
          }
          else
          {
            /* Đưa data vào buffer */
            ring_buffer.Enqueue(fsm_valueAll.data_out[ECG_WAVES_DATA_INDEX]);
            flag_downsample = true;
          }
        }
      }
    }
  }
}

void ECG_Init(void)
{
  /* Init serial */
  Serial.begin(115200);
  /* 18 - Rx | 17 - Tx */
  uart1.begin(115200, SERIAL_8N1, 18, 17);
  /* Config Timer */
  TIMER_Init();
  /* Init Button */
  BUTTON_Init();
  /* Init SD Card */
  SDCARD_Init();
  /* Init ECG Model */
  ECG_ModelInit();
  /* Init ECG FSM */
  fsm.ECG_ClearTimeOut_Fsm();

  delay(1000);
}

void ECG_Run(void)
{
  // Send message JSON to Serial Monitor
  if(count_time_send_msg >= 1500)
  {
    // Serial.println("--- Send Message JSON ---");
    ECG_SendMesgJson();
    count_time_send_msg = 0;
  }

  if(ring_buffer.Get_Head() > LEN_FRAME_ROTATION)
  {
    if(flag_start == false)
    {
      /* Tìm giá trị lớn nhất và vị trí của giá trị lớn nhất */
      result = ring_buffer.MaxAndIndex(start_index, LEN_FRAME_ROTATION);
      /* Lưu vị trí đỉnh đầu tiên */
      max_index_start = result.second;

      /* Từ đỉnh đầu tiên tìm đỉnh thứ 2 */
      if(ring_buffer.Get_Head() > max_index_start + LEN_FRAME_ROTATION)
      {
        #if DEBUG_PRINT
        Serial.print("Get_Head: ");
        Serial.println(ring_buffer.Get_Head());
        Serial.print("max_index_start: ");
        Serial.println(max_index_start);
        #endif
        
        start_index = max_index_start + 10;
        /* Tìm giá trị lớn nhất và vị trí của giá trị lớn nhất */
        result = ring_buffer.MaxAndIndex(start_index, LEN_FRAME_ROTATION);
        /* Vị trí của đỉnh thứ 2 */
        max_index_next = result.second;
        #if DEBUG_PRINT
        Serial.print("max_index_next: ");
        Serial.println(max_index_next);
        #endif

        flag_start = true;
      }
    }

    if(start_index + 170 >= ring_buffer.Get_Capacity() || max_index_start + 170 >= ring_buffer.Get_Capacity())
    {
      start_index = 0;
      flag_start = false;
      ring_buffer.Reset_RingBuffer();
      // Serial.println("=================================== Reset Ring Buffer ====================================");
    }
    else if(ring_buffer.Get_Head() > max_index_next + 70 && flag_start == true)
    {
      flag_start = false;

      len_frame_data = (max_index_next - max_index_start)*1.2;
      // Serial.print("len_frame_data: ");
      // Serial.println(len_frame_data);
      ring_buffer.Set_Beginning_Index(max_index_start);
      ring_buffer.Get_Array(arr_frame_buffer, len_frame_data);
      start_index = max_index_next;

      time_start_ECG_Derivative = millis();
      
      /* Process data arrays */
      ECG_Derivative();

      time_end_ECG_Derivative = millis();
      #if DEBUG_TIME_RUNNING
      Serial.print("Time running ECG_Derivative: ");
      Serial.println(time_end_ECG_Derivative - time_start_ECG_Derivative);
      #endif

      /* Run ECG Model */
      ECG_ModelRun();

      /* Memset arr_RQS */
      memset(arr_QRS, 0, sizeof(arr_QRS));
      /* Memset arr_data_ecg */
      memset(arr_data_ecg, 0, sizeof(arr_data_ecg));
      /* Memset arr_derivative */
      memset(arr_derivative, 0, sizeof(arr_derivative));
      /* Memset arr_frame_buffer */
      memset(arr_frame_buffer, 0, sizeof(arr_frame_buffer));

      Serial.println();
    }
  }
}

void ECG_Derivative(void)
{
  /* Convert arr_frame_buffer with variable 255 */
  ECG_Normalize();

  /* Copy data from arr_frame_buffer to arr_data_ecg */
  for(uint16_t i = 0; i < LEN_ARR_DATA_ECG; i++)
  {
    if(i < len_frame_data)
    {
      arr_data_ecg[i] = arr_frame_buffer[i];
      // Serial.print(arr_data_ecg[i]);
      // Serial.print(" ");
    }
    else
    {
      arr_data_ecg[i] = 0;
      // Serial.print(arr_data_ecg[i]);
      // Serial.print(" ");
    }
  }
  // Serial.println();
  // Serial.println();

  /* Arr Derivative */
  // Serial.println("--- Arr Derivative ---");
  for(uint8_t i = 0; i < LEN_ARR_DATA_ECG - 1; i++)
  {
    arr_derivative[i] = arr_data_ecg[i + 1] - arr_data_ecg[i];
    #if DEBUG_PRINT
    Serial.print(arr_derivative[i]);
    Serial.print(" ");
    #endif
  }
  // Serial.println();

  /* Find index and variable R */
  index_R = std::distance(arr_data_ecg, std::max_element(arr_data_ecg + 50, arr_data_ecg + LEN_ARR_DATA_ECG));
  var_R = arr_data_ecg[index_R];
  #if DEBUG_PRINT
  Serial.print("index_R: ");
  Serial.print(index_R);
  Serial.print(" var_R: ");
  Serial.println(var_R);
  #endif

  /* Find index and variable Q */
  for(uint8_t i = index_R - 1; i > 0; i--)
  {
    if(arr_derivative[i] < 0)
    {
      index_Q = i+1;
      var_Q = arr_data_ecg[i+1];
      #if DEBUG_PRINT
      Serial.print("index_Q: ");
      Serial.print(index_Q);
      Serial.print(" var_Q: ");
      Serial.println(var_Q);
      #endif
      break;
    }
  }

  /* Find index and variable S */
  for(uint8_t i = index_R; i < LEN_ARR_DATA_ECG; i++)
  {
    if(arr_derivative[i] > 0)
    {
      index_S = i;
      var_S = arr_data_ecg[i];
      #if DEBUG_PRINT
      Serial.print("index_S: ");
      Serial.print(index_S);
      Serial.print(" var_S: ");
      Serial.println(var_S);
      #endif
      break;
    }
  }

  /* Save QRS */
  arr_QRS[0] = index_R;
  arr_QRS[1] = var_R;
  arr_QRS[2] = index_Q;
  arr_QRS[3] = var_Q;
  arr_QRS[4] = index_S;
  arr_QRS[5] = var_S;
}

void ECG_Normalize(void)
{
  uint16_t max_value = *std::max_element(arr_frame_buffer, arr_frame_buffer + len_frame_data);
  float multiplier = 255.0f / max_value; // Ép kiểu 255 thành float
  #if DEBUG_PRINT
  Serial.print("multiplier: ");
  Serial.println(multiplier);
  #endif
  for(uint16_t i = 0; i < len_frame_data; i++)
  {
    arr_frame_buffer[i] = multiplier * arr_frame_buffer[i];
  }
}

void ECG_ModelInit(void)
{
  // Serial.println("--- Map the model into a usable data structure ---");
  tflModel = tflite::GetModel(ecg_model_quant6_tflite);
  if (tflModel->version() != TFLITE_SCHEMA_VERSION) {
    Serial.println("Model schema mismatch!");
    while (1);
  }

  tflite::MicroErrorReporter tflErrorReporter;
  static tflite::AllOpsResolver resolver;
  tflInterpreter = new tflite::MicroInterpreter(tflModel, resolver, tensorArena, tensorArenaSize, &tflErrorReporter);
  tflInterpreter->AllocateTensors();
  tflInput1Tensor = tflInterpreter->input(0);
  tflInput2Tensor = tflInterpreter->input(1);
  tflOutputTensor = tflInterpreter->output(0);
}

void ECG_ModelRun(void)
{
  time_start_ECG_ModelRun = millis();

  // Serial.println("--- Run the model ---");
  for (int i = 0; i < 187; i++)
  {
    tflInput1Tensor->data.int8[i] = arr_data_ecg[i];
  }

  for (int i = 0; i < 6; i++)
  {
    tflInput2Tensor->data.int8[i] = arr_QRS[i];
  }

  // Run inference
  tflInterpreter->Invoke();
  // Get the results
  int8_t* output = tflOutputTensor->data.int8;

  time_end_ECG_ModelRun = millis();
  #if DEBUG_TIME_RUNNING
  Serial.print("Time running ECG_ModelRun: ");
  Serial.println(time_end_ECG_ModelRun - time_start_ECG_ModelRun);
  #endif

  // Find the maximum value in the output
  int8_t index_max_positive = 0;
  int8_t max_positive_value = std::numeric_limits<int8_t>::min();
  for (int i = 0; i < 4; i++) 
  {
    if (output[i] > max_positive_value && output[i] > 0) 
    {
      max_positive_value = output[i];
    }
  }

  if(max_positive_value != std::numeric_limits<int8_t>::min()) 
  {
    index_max_positive = std::distance(output, std::find(output, output + 4, max_positive_value));
    #if DEBUG_PRINT
    Serial.print("Max positive value: ");
    Serial.println(max_positive_value);
    Serial.print("Index max positive: ");
    Serial.println(index_max_positive);
    #endif
  }

  // Count N, S, V, F, Q
  switch (index_max_positive)
  {
    case 0:
      count_N++;
    break;

    case 1:
      count_S++;
      filename_S = "/S" + std::to_string(count_filename_S) + ".txt"; // tạo tên tệp
      ECG_SDCardWrite(filename_S); // ghi dữ liệu vào tệp
      count_filename_S++; // tăng số lượng tệp
    break;

    case 2:
      count_V++;
      filename_V = "/V" + std::to_string(count_filename_V) + ".txt";
      ECG_SDCardWrite(filename_V);
      count_filename_V++;
    break;

    case 3:
      count_F++;
      filename_F = "/F" + std::to_string(count_filename_F) + ".txt";
      ECG_SDCardWrite(filename_F);
      count_filename_F++;
    break;

    case 4:
      count_Q++;
      filename_Q = "/Q" + std::to_string(count_filename_Q) + ".txt";
      ECG_SDCardWrite(filename_Q);
      count_filename_Q++;
    break;

    default:
      break;
  } 
}

void ECG_SDCardWrite(std::string file_name)
{
  time_start_ECG_SDCardWrite = millis();

  File file = SD.open(file_name.c_str(), FILE_WRITE);
  if (!file) 
  {
    Serial.println("Failed to open file for writing");
    return;
  }

  for(uint16_t i = 0; i < LEN_ARR_DATA_ECG; i++)
  {
    file.print(arr_data_ecg[i]);
    file.print(" ");
  }
  file.println();
  file.close();

  time_end_ECG_SDCardWrite = millis();
  #if DEBUG_TIME_RUNNING
  Serial.print("Time running ECG_SDCardWrite: ");
  Serial.println(time_end_ECG_SDCardWrite - time_start_ECG_SDCardWrite);
  #endif
}

void ECG_SendMesgJson(void)
{
  ECG_Button();
  
  if(var_ButtonStop == 0)
  {
    doc["N"]      = count_N;
    doc["S"]      = count_S;
    doc["V"]      = count_V;
    doc["F"]      = count_F;
    doc["Q"]      = count_Q;
    doc["Stop"]   = var_ButtonStop;
    doc["Reset"]  = var_ButtonReset;

    serializeJson(doc, Serial);
    // Kết thúc chuỗi JSON
    Serial.println(); 
  }

  var_ButtonReset = 0;
}

void ECG_Button(void)
{
  if(flag_start_button == 0)
  {
    button_1.var_FlagChange = 1;
    button_2.var_FlagChange = 1;
    flag_start_button = 1;
  }

  // Button 1 Reset ECG
  if(button_1.var_FlagChange == 0)
  {
    count_F = 0;
    count_N = 0;
    count_Q = 0;
    count_S = 0;
    count_V = 0;
    
    var_ButtonStop = 0;
    var_ButtonReset = 1;
    button_1.var_FlagChange = 1;
    // Serial.println("Button 1 pressed");
  }

  // Button 2 Stop ECG
  if(button_2.var_FlagChange == 0)
  {
    var_ButtonStop = 1;
    button_2.var_FlagChange = 1;
    // Serial.println("Button 2 pressed");
  }
}

void TIMER_Init(void)
{
  My_timer = timerBegin(0, 80, true);
  timerAttachInterrupt(My_timer, &onTimer, true);
  timerAlarmWrite(My_timer, 1000, true);
  timerAlarmEnable(My_timer);
}
