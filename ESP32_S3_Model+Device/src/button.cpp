#include "button.h"

TS_TypeInput button_1;
TS_TypeInput button_2;

void BUTTON_Init(void)
{
    pinMode(PIN_BUTTON_1, INPUT);
    pinMode(PIN_BUTTON_2, INPUT);
    button_1.var_FlagChange = 1;
    button_2.var_FlagChange = 1;    
}

void BUTTON_Readall(void)
{
  BUTTON_Interrupt(BUTTON_1_READ, (TS_TypeInput*)(&button_1));
  BUTTON_Interrupt(BUTTON_2_READ, (TS_TypeInput*)(&button_2));
}

void BUTTON_Interrupt(unsigned char var_Input, TS_TypeInput *button)
{
    button->var_DataOld = var_Input;
    if(!var_Input)
    {
        if(button->var_DataOld == var_Input)
        {
            if(button->var_CountAccess <= 1000)
            {
                button->var_CountAccess++;
                if(button->var_CountAccess == 20)
                {
                    button->var_FlagChange = 0;
                }
            }
        }
        else
        {
            button->var_CountAccess = 0;
        }
    }
    else
    {
        button->var_CountAccess = 0;
    }
}
