#include "ecg_message.h"


uint8_t ecg_message::ECG_Message_Create_Frame(ecg_FrameMsg_t data_in, uint8_t *data_out)
{
    uint8_t count = 0;

    if(data_in.StartFrame != START_BYTE)
    {
        return 0;
    }

    data_out[count] = (uint8_t)(data_in.StartFrame & 0xFF);
    count += 1;
    data_out[count] = (uint8_t)(data_in.StartFrame >> 8);
    count += 1;
    data_out[count] = data_in.LengthData;
    count += 1;

    for (uint8_t i = 0; i < data_in.LengthData - 2; i++)
    {
        data_out[count] = data_in.Data[i];
        count += 1;
    }

    data_in.CheckFrame = ECG_CheckSum(data_out, data_in.LengthData + 2);
    data_out[count] = data_in.CheckFrame;
    count += 1;

    return count;
}

uint8_t ecg_message::ECG_Message_Detect_Frame(uint8_t *data_in, ecg_FrameMsg_t *data_out)
{
    uint8_t count = 0;

    data_out->StartFrame = ECG_Convert_From_Bytes_To_Uint16(data_in[count], data_in[count+1]);
    count += 2;
    data_out->LengthData = data_in[count];
    count += 1;

    for (uint8_t i = 0; i < data_out->LengthData - 2; i++)
    {
        data_out->Data[i] = data_in[count];
        count += 1;
    }

    data_out->CheckFrame = data_in[count];

    return 1;
}

uint8_t ecg_message::ECG_CheckSum(uint8_t *buf, uint8_t len)
{
    uint8_t sum = 0;
    for (uint8_t i = 2; i < len-1; i++)
    {
        sum = (sum + buf[i]) % 256;
    }
    sum = 0xFF - sum;
    return sum;
}

void ecg_message::ECG_Send_DataToUart0(uint8_t data)
{
    Serial.println(data, HEX);
}

void ecg_message::ECG_Print_Frame_Send(uint8_t *data, uint8_t len)
{
    Serial.print("Start Frame: ");
    Serial.println(ECG_Convert_From_Bytes_To_Uint16(data[0], data[1]), HEX);
    Serial.print("Length Data: ");
    Serial.println(data[2], HEX);
    Serial.print("Data:        ");
    for (uint8_t i = 3; i < len - 1; i++)
    {
        Serial.print(data[i], HEX);
        Serial.print(" ");
    }
    Serial.println();
    Serial.print("Check Frame: ");
    Serial.println(data[len - 1], HEX);
    Serial.println();
}

void ecg_message::ECG_Print_Frame_Rec(ecg_FrameMsg_t *data, uint8_t len)
{
    Serial.print("Start Frame: ");
    Serial.println(data->StartFrame, HEX);
    Serial.print("Length Data: ");
    Serial.println(data->LengthData, HEX);
    Serial.print("Data:        ");
    for (uint8_t i = 0; i < data->LengthData - 2; i++)
    {
        Serial.print(data->Data[i], HEX);
        Serial.print(" ");
    }
    Serial.println();
    Serial.print("Check Frame: ");
    Serial.println(data->CheckFrame, HEX);
    Serial.println();
}