/**
 * @brief Construct a new ring buffer object
 *
 * @param buf
 * @param buf_size
 */
template <typename T>
RING_BUFFER<T>::RING_BUFFER(T *buf, int buf_size)
{
  this->buffer = buf;
  this->capacity = buf_size;
  this->buffer_mask = buf_size - 1;
  this->tail_index = 0;
  this->head_index = 0;

  this->beginning_index = 0;
  this->ending_index = 0;
  this->counter = 0;
}

/**
 * @brief Initializes the ring buffer pointed to by buffer
 * This function can also be used to empty/reset the buffer.
 * The resulting buffer can contain buf_size-1 <T>.
 * @param buf The buffer allocated for the ring buffer.
 * @param buf_size The size of the allocated ring buffer.
 */
template <typename T>
void RING_BUFFER<T>::Init(T *buf, int buf_size)
{
  this->buffer = buf;
  this->capacity = buf_size;
  this->buffer_mask = buf_size - 1;
  this->tail_index = 0;
  this->head_index = 0;

  this->beginning_index = 0;
  this->ending_index = 0;
  this->counter = 0;
}

/**
 * Adds a <T>data to a ring buffer.
 * @param data The data to push.
 */
template <typename T>
void RING_BUFFER<T>::Enqueue(T data)
{
  /* Is buffer full? */
  if (this->Is_Full() == 1)
  {
    /* Overwrite the oldest value */
    /* Increase tail index */
    this->tail_index = (this->tail_index + 1);
    if (this->tail_index > this->buffer_mask)
    {
      this->tail_index = 0;
    }
  }
  else
  {
    /* Only increase the counter if the buffer is not full */
    this->counter = this->counter + 1;
  }

  /* Put data in buffer */
  this->buffer[this->head_index] = data;
  this->head_index = (this->head_index + 1);

  /* Check if the head index is bigger than the allocated size */
  if(this->head_index > this->buffer_mask)
  {
    this->head_index = 0;
  }

  if (this->counter % NUMS_IN_WINDOW == 0)
  {
    this->window_full = true;
  }
}

/**
 * Adds an array of <T>data to a ring buffer.
 * @param data A pointer to the array of <T> to Put in the queue.
 * @param len The size of the array.
 */
template <typename T>
void RING_BUFFER<T>::Enqueue_Array(T *data, int len)
{
  /* Add one by one */
  int i;
  for (i = 0; i < len; i++)
  {
    Enqueue(data[i]);
  }
}

/**
 * Returns the oldest <T>data in a ring buffer.
 * @param data A pointer to the location at which the data should be Put.
 * @return 1 if data was returned; 0 otherwise.
 */
template <typename T>
int RING_BUFFER<T>::Dequeue(T *data)
{
  if (this->Is_Empty() == 1)
  {
    /* No items */
    return 0;
  }
  else
  {
    /* Only decrease the counter if the buffer is not empty */
    this->counter = this->counter - 1;
  }

  *data = this->buffer[this->tail_index];
  this->tail_index = (this->tail_index + 1);
  /* Check if the tail index is bigger than the allocated size */
  if (this->tail_index > this->buffer_mask)
  {
    this->tail_index = 0;
  }

  return 1;
}

/**
 * Returns the <em>len</em> oldest <T>data in a ring buffer.
 * @param data A pointer to the array at which the data should be Put.
 * @param len The maximum number of data to return.
 * @return -1 if required length is invalid; 0 if buffer is empty; otherwise The number of <T>data returned.
 */
template <typename T>
int RING_BUFFER<T>::Dequeue_Array(T *data, int len)
{
  if (this->Is_Empty() == 1)
  {
    /* No items */
    return 0;
  }

  if (len > this->Count())
  {
    return -1;
  }

  T *data_ptr = data;
  int cnt = 0;
  while ((cnt < len) && Dequeue(data_ptr))
  {
    cnt++;
    (data_ptr)++;
  }
  return cnt;
}

/**
 * Peeks a ring buffer, i.e. returns an element without removing it.
 * @param data A pointer to the location at which the data should be gotten.
 * @param index The index to peek.
 * @return 1 if data was returned; 0 otherwise.
 */
template <typename T>
int RING_BUFFER<T>::Peek(T *data, int index)
{
  if (this->tail_index + index >= this->Count())
  {
    /* No items at index */
    return 0;
  }

  /* Add desired index to the tail index */
  int data_index = (this->tail_index + index) % this->capacity;
  *data = this->buffer[data_index];
  return 1;
}

/**
 * Returns the <em>len</em> oldest <T>data in a ring buffer without changing head and tail index.
 * @param data A pointer to the array at which the data should be Put.
 * @param len The maximum number of data to return.
 * @return -1 if required length is invalid; 0 if buffer is empty; otherwise The number of <T>data returned.
 */
template <typename T>
int RING_BUFFER<T>::Get_Array(T *data, int len)
{
  if (this->Is_Empty() == 1)
  {
    /* No items */
    return 0;
  }

  if (len > this->Count())
  {
    return -1;
  }

  int index = this->beginning_index;
  T *data_ptr = data;
  int cnt = 0;
  while ((cnt < len) && Peek((data_ptr), index))
  {
    index++;
    cnt++;
    (data_ptr)++;
  }
  // data = (this->buffer + index);

  if (this->beginning_index >= this->capacity)
  {
    this->beginning_index = this->beginning_index % this->capacity;
  }

  return cnt;
}

/**
 * @brief Set the beginning index to get array from buffer
 *
 * @param index
 */
template <typename T>
void RING_BUFFER<T>::Set_Beginning_Index(int index)
{
  this->beginning_index = index;
}

/**
 * @brief Get the beginning index to extract array from buffer
 *
 * @return int
 */
template <typename T>
int RING_BUFFER<T>::Get_Beginning_Index()
{
  return this->beginning_index;
}

template <typename T>
bool RING_BUFFER<T>::Get_Window_State()
{
  return this->window_full;
}

template <typename T>
void RING_BUFFER<T>::Set_Window_State(bool state)
{
  this->window_full = state;
}


template <typename T>
std::pair<T, int> RING_BUFFER<T>::MaxAndIndex(int start, int align)
{
  int max_index = -1;
  T max_value = std::numeric_limits<T>::min();

  for (int i = start; i < start + align - 2; i++)
  {
    int index = i % this->capacity;
    if (this->buffer[i] - this->buffer[i+2] > max_value)
    {
      max_value = this->buffer[i] - this->buffer[i+2];
      max_index = index;
    }
  }
  
  return {max_value, max_index};
}

/**
 * @brief Get the number of elements in the ring buffer.
 * @return The number of elements in the ring buffer.
 */
template <typename T>
void RING_BUFFER<T>::Reset_RingBuffer()
{
  this->head_index = 0;
  this->tail_index = 0;
  this->counter = 0;
  this->beginning_index = 0;
  this->ending_index = 0;
  this->window_full = false;
}
