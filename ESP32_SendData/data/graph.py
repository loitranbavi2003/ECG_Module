# Số lượng phần tử trong file
with open('F:\ECG_Module\Detect_Data_ECG\Cmake_sample\data\data_ecg_1.csv', 'r') as file:
    data = file.read()
    elements = data.split(",")
    num_elements = len(elements)

print(f'The file contains {num_elements} elements.')