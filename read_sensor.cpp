/**
 ******************************************************************************
 * @file    read_sensor.cpp
 * @author  DonNV
 * @version V1.0
 * @date
 * @brief  This file provides firmware functions to manage the following
 *          functionalities of read data sensor.
 ******************************************************************************
 */

#include "../header/read_sensor.h"
#include <QDebug>
using namespace std;
/**
 * @brief This is function used to calculator check sum.
 *
 * @param pkg is package need calculator check sum.
 * @param len is length of pkg
 * @retval none
 * @note SUM = ~（N+A1+A2+...+An）, “~” means NOT (Negation operator)，1 byte.
 */

unsigned char read_sensor::calculator_sum(unsigned char *pkg, int len)
{
    int i;
    unsigned char sum = 0;

    for (i = 2; i < len - 1; i++)
    {
        sum += pkg[i];
    }

    return (unsigned char)((0xFF - sum));
}

unsigned char read_sensor::spo2_data()
{
    return spo2;
}

unsigned char read_sensor::pr_data()
{
    return pr;
}

unsigned char read_sensor::spo2_wave()
{
    return spo2_wave_point;
}

unsigned char read_sensor::status_connect_spo2()
{
    return status_spo2;
}

unsigned char read_sensor::spo2_pic()
{
    return pic_spo2_status;
}

unsigned char read_sensor::hr_ecg_pic()
{
    return pic_hr_status;
}

unsigned char read_sensor::cuff_data()
{
    return cuff;
}

unsigned char read_sensor::sys_data()
{
    return sys;
}

unsigned char read_sensor::mean_data()
{
    return mean;
}

unsigned char read_sensor::dia_data()
{
    return dia;
}

unsigned char read_sensor::status_nibp_process()
{
    return status_nibp;
}

double read_sensor::temperature_data()
{
    temperature = temperature_inte + (double)temperature_deci / 10;
    return temperature;
}

unsigned char read_sensor::status_connect_temperature()
{
    return status_temperature;
}

unsigned char read_sensor::ecg_wave()
{
    return ecg_wave_point;
}

unsigned char read_sensor::ecg_heart_data()
{
    return ecg_heart_rate;
}

unsigned char read_sensor::ecg_resp_rate_data()
{
    return ecg_resp_rate;
}

unsigned char read_sensor::status_ecg_process()
{
    return ecg_status;
}

unsigned char read_sensor::resp_wave()
{
    return resp_wave_point;
}

unsigned char read_sensor::ecg_leadII_wave()
{
    return ecg_lead2_wave_point;
}

unsigned char read_sensor::ecg_leadIII_wave()
{
    return ecg_lead3_wave_point;
}

unsigned char read_sensor::ecg_leadaVR_wave()
{
    return ecg_leadaVR_wave_point;
}

unsigned char read_sensor::ecg_leadaVL_wave()
{
    return ecg_leadaVL_wave_point;
}

unsigned char read_sensor::ecg_leadaVF_wave()
{
    return ecg_leadaVF_wave_point;
}

unsigned char read_sensor::ecg_leadV_wave()
{
    return ecg_leadV_wave_point;
}

/**
 * @brief  This is function used to parse data from sensor.
 *         will analyze the package giving the parameters of each type of sensor(NIBP, SPO2, ECG, TEMP).
 * @param  pkgdata is buffer data after receiver from data.
 * @param  pkglength is length of pkgdata.
 * @retval None
 * @note  parse_package is function static and not user other file.
 */

void read_sensor::parse_package(unsigned char *pkgdata, unsigned int pkglength)
{
    unsigned char pkgtype;
    //    unsigned char version[8];
    string str;
    pkgtype = pkgdata[3];

    switch (pkgtype)
    {
    case PKG_ECG_WAVE:
    {
        ecg_wave_point = pkgdata[4];
        ecg_lead2_wave_point = pkgdata[5];
        ecg_lead3_wave_point = pkgdata[6];
        ecg_leadaVR_wave_point = pkgdata[7];
        ecg_leadaVL_wave_point = pkgdata[8];
        ecg_leadaVF_wave_point = pkgdata[9];
        ecg_leadV_wave_point = pkgdata[10];
        break;
    }
    case PKG_ECG_PARAM:
    {
        ecg_status = (unsigned int)pkgdata[4];
        ecg_heart_rate = (unsigned int)pkgdata[5];
        ecg_resp_rate = (unsigned int)pkgdata[6];
        break;
    }
    case PKG_NIBP:
    {
        status_nibp = (unsigned int)pkgdata[4];
        cuff = (unsigned int)pkgdata[5];
        sys = (unsigned int)pkgdata[6];
        mean = (unsigned int)pkgdata[7];
        dia = (unsigned int)pkgdata[8];

        break;
    }
    case PKG_SPO2:
    {
        status_spo2 = (unsigned int)pkgdata[4];
        spo2 = pkgdata[5];
        pr = pkgdata[6];
        break;
    }
    case PKG_TEMP:
    {
        status_temperature = (unsigned int)pkgdata[4];
        temperature_inte = pkgdata[5];
        temperature_deci = pkgdata[6];
        break;
    }
    case PKG_SW_VER:
    {
        // char softwave_version[9];
        // for (int i = 4; i < pkglength - 1; i++)
        // {
        //     softwave_version[i-4] = pkgdata[i];
        // }
        break;
    }
    case PKG_HW_VER:
    {
        // char hardware_vesion[4];
        // for (int i = 4; i < pkglength - 1; i++)
        // {
        //     hardware_vesion[i-4] = pkgdata[i];
        // }
        break;
    }
    case PKG_SPO2_WAVE:
    {
        spo2_wave_point = pkgdata[4];
        break;
    }
    case PKG_SPO2_PEAK:
    {
        pic_spo2_status = (unsigned int)pkgdata[4];
        break;
    }
    case PKG_HR_ECG_PEAK:
    {
        pic_hr_status = (unsigned int)pkgdata[4];
        break;
    }
    case PKG_RESP_WAVE:
    {

        resp_wave_point = (unsigned int)pkgdata[4];
        break;
    }
    default:
        break;
    }
}

/**
 * @brief  This is function used to receiver data from sensor.
 * @param  uart0_filestream is flag used to check state of serial port.
 * @retval None
 */
int read_sensor::receiver_data_queue(int *uart0_filestream)
{
    static unsigned char buffer[512];
    static unsigned int parseIndex = 0;
    static unsigned int emptyIndex = 0;
    unsigned char readBuff[512];
    unsigned int count = 0, i, j;
    unsigned char pkgData[512];
    bool pkgStart;
    unsigned char pkgIndex;
    unsigned int pkgLength;
    count_time++;
    if (count_time % 15 == 0)
    {
        pic_spo2_status = 0;
        pic_hr_status = 0;
    }
    memset(buffer, 0, BUFFER_SIZE);
    if (*uart0_filestream != -1)
    {
        memset(readBuff, 0, sizeof(readBuff));
        memset(pkgData, 0, sizeof(pkgData));

        count = read(*uart0_filestream, readBuff, 512);
        if (count <= 0)
        {
            return -1;
        }
        for (i = 0; i < count; i++)
        {
            buffer[emptyIndex] = readBuff[i];
            if ((emptyIndex + 1) < sizeof(buffer))
            {
                emptyIndex = emptyIndex + 1;
            }

            else
            {
                emptyIndex = 0;
            }

            if (emptyIndex == parseIndex)
            {
                parseIndex = 0;
                emptyIndex = 0;
                return -1;
            }
        }
        i = parseIndex;
        count = 0;
        do
        {

            if (i != parseIndex && buffer[i] == 0x55)
            {
                ;
            }
            if ((i + 1) < sizeof(buffer))
            {
                i = i + 1;
            }

            else
            {
                i = 0;
            }
            count++;
        } while (i != emptyIndex);

        pkgStart = false;
        pkgIndex = 0;
        i = parseIndex;
        while (i != emptyIndex)
        {
            if (buffer[i] == 0x55)
            {
                if ((i + 1) < sizeof(buffer))
                {
                    j = i + 1;
                }

                else
                {
                    j = 0;
                }

                if (j != emptyIndex && buffer[j] == 0xaa)
                {

                    pkgStart = true;
                    pkgIndex = 0;
                    pkgLength = 0;
                    parseIndex = i;
                }
            }

            if (pkgStart)
            {
                pkgData[pkgIndex] = buffer[i];
                if (++pkgIndex == 3)
                {
                    pkgLength = buffer[i];
                }

                if ((pkgLength != 0) && (pkgIndex == pkgLength + 2))
                {

                    if (calculator_sum(pkgData, pkgIndex) == pkgData[pkgIndex - 1])
                    {
                        parse_package(pkgData, pkgIndex);
                        memset(pkgData, 0, sizeof(pkgData));
                    }
                    else
                    {
                        // printf("check sum error \n");
                    }

                    pkgStart = false;
                    if ((i + 1) < sizeof(buffer))
                    {
                        parseIndex = i + 1;
                    }

                    else
                    {
                        parseIndex = 0;
                    }
                }
            }

            if ((i + 1) < sizeof(buffer))
            {
                i = i + 1;
            }

            else
            {
                i = 0;
            }
        }
    }
    else
    {
        printf("error\n");
    }
    return 0;
}

// EOF
