#ifndef _SD_CARD_H_
#define _SD_CARD_H_

#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include <Arduino.h>

#define SD_CS 10 // Change to your CS pin

#define READ_SDCARD 0
#define WRITE_SDCARD 0

void SDCARD_Init(void);

#endif
