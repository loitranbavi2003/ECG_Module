#ifndef _ECG_DEFINE_H_
#define _ECG_DEFINE_H_

#include <Wire.h>
#include <string>
#include <vector>
#include <stdint.h>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <Arduino.h>
#include <ArduinoJson.h>
#include <HardwareSerial.h>

#include "button.h"
#include "sd_card.h"
#include "ecg_fsm.h"
#include "ringbuffer.h"
#include "ecg_message.h" 
#include "ecg_convert.h"

#define DEBUG_PRINT 0
#define DEBUG_TIME_RUNNING 0

#define LEN_ARR_DATA_ECG 187
#define LEN_FRAME_ROTATION 90

void ECG_Init(void);
void ECG_Run(void);
void ECG_Button(void);
void ECG_Derivative(void);
void ECG_SendMesgJson(void);
void ECG_SDCardWrite(std::string file_name);

void ECG_ModelInit(void);
void ECG_ModelRun(void);
void ECG_Normalize(void);

void TIMER_Init(void);

#endif 
