#include "ecg_fsm.h"

ecg_message mesg;
fsm_valueAll_t fsm_valueAll;
fsm_state_e fsm_state = FSM_STATE_START_FRAME;

static uint8_t  Check_Frame = 0;
static uint8_t  CheckFrame_Receive = 0;


void ecg_fsm::ECG_TimeOut_Fsm(void)
{
    
}

void ecg_fsm::ECG_ClearTimeOut_Fsm(void)
{
    fsm_valueAll.count_array = 0;
    fsm_state = FSM_STATE_START_FRAME;
}

uint8_t ecg_fsm::ECG_RecMessage_Fsm(uint8_t temp_data)
{	
	switch (fsm_state)
    {
        case FSM_STATE_START_FRAME:
            fsm_valueAll.data_out[fsm_valueAll.count_array] = temp_data;
            fsm_valueAll.count_array++;

            if(fsm_valueAll.count_array == 1 && fsm_valueAll.data_out[0] != 0x55)
            {
                fsm_valueAll.count_array = 0;
            }
            else if(fsm_valueAll.count_array == 2 && fsm_valueAll.data_out[1] != 0xAA)
            {
                fsm_valueAll.count_array = 0;
            }
            
            if(fsm_valueAll.count_array == FSM_STATE_CHANGE_VALUE_LENGHT_DATA)
            {
                fsm_state = FSM_STATE_LENGHT_DATA;
            }
        break;

        case FSM_STATE_LENGHT_DATA:
            fsm_valueAll.data_out[fsm_valueAll.count_array] = temp_data;
            fsm_valueAll.count_array++;

            if (fsm_valueAll.count_array == FSM_STATE_CHANGE_VALUE_END)
            {
                fsm_valueAll.data_length = fsm_valueAll.data_out[2] + 2;

                if (fsm_valueAll.data_length < 50)
                { 
                    fsm_state = FSM_STATE_END;
                }
                else if (fsm_valueAll.data_length > 50)
                {
                    fsm_valueAll.count_array = 0;
                    fsm_state = FSM_STATE_START_FRAME;
                }
            }
        break;

        case FSM_STATE_END:
            fsm_valueAll.data_out[fsm_valueAll.count_array] = temp_data;
            fsm_valueAll.count_array++;

            if (fsm_valueAll.count_array == fsm_valueAll.data_length)
            {
				Check_Frame = mesg.ECG_CheckSum(fsm_valueAll.data_out, fsm_valueAll.data_length);
                CheckFrame_Receive = fsm_valueAll.data_out[fsm_valueAll.data_length - 1];
				
                if(Check_Frame == CheckFrame_Receive)
                {
                    ECG_ClearTimeOut_Fsm();
                    return 1;
                }
                else
                {
                    ECG_ClearTimeOut_Fsm();
                    return 0;
                }
            }
        break;
    }
	
    return 0;
}
