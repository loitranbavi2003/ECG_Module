#ifndef _ECG_FSM_H_
#define _ECG_FSM_H_

#include "ecg_define.h"


// #define FSM_STATE 5 
// #define FSM_TIMEOUT 50

typedef enum
{
    FSM_STATE_START_FRAME  = 0,
    FSM_STATE_LENGHT_DATA  = 1,
    FSM_STATE_END          = 2
} fsm_state_e;

typedef enum
{
    FSM_STATE_CHANGE_VALUE_START_FRAME  = 0,
    FSM_STATE_CHANGE_VALUE_LENGHT_DATA  = 2,
    FSM_STATE_CHANGE_VALUE_END          = 3,
} fsm_state_change_value_e;

typedef struct 
{
    uint8_t temp_data;
    uint8_t data_out[100];
    uint8_t count_array;
    uint8_t data_length;
    uint8_t Flag_Fsm;
    uint8_t Flag_fsm_True;
}fsm_valueAll_t;
extern fsm_valueAll_t fsm_valueAll;

class ecg_fsm
{
    public:
        void ECG_TimeOut_Fsm(void);
        void ECG_ClearTimeOut_Fsm(void);
        uint8_t ECG_RecMessage_Fsm(uint8_t temp_data);
};

#endif 