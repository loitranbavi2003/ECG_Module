#ifndef _BUTTON_H_
#define _BUTTON_H_

#include <Arduino.h>

#define PIN_BUTTON_1 20
#define PIN_BUTTON_2 21

#define BUTTON_1_READ digitalRead(PIN_BUTTON_1)
#define BUTTON_2_READ digitalRead(PIN_BUTTON_2)

typedef struct
{
  unsigned char	var_DataOld;
  unsigned int 	var_CountAccess;
  unsigned char var_FlagChange;
  unsigned char var_hold;
} TS_TypeInput;
extern TS_TypeInput button_1;
extern TS_TypeInput button_2;

void BUTTON_Init(void);
void BUTTON_Readall(void);
void BUTTON_Interrupt (unsigned char _var_Input, TS_TypeInput *_vrts_DataInput);

#endif
