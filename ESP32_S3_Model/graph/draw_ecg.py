import matplotlib.pyplot as plt

# Chuỗi chứa dữ liệu ECG
str1 = "158 122 128 132 132 132 134 133 133 134 134 135 135 136 136 138 140 139 142 147 146 145 141 141 137 136 136 133 133 131 131 132 130 130 130 130 131 131 131 132 131 132 132 131 133 132 132 132 131 131 130 130 130 130 131 130 130 129 129 130 129 130 132 132 133 130 131 130 129 132 131 131 133 131 131 130 130 136 154 155 124 129 130 129 131 132 131 133 133 134 135 135 137 136 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0";
str2 = "-36 6 4 0 0 2 -1 0 1 0 1 0 1 0 2 2 -1 3 5 -1 -1 -4 0 -4 -1 0 -3 0 -2 0 1 -2 0 0 0 1 0 0 1 -1 1 0 -1 2 -1 0 0 -1 0 -1 0 0 0 1 -1 0 -1 0 1 -1 1 2 0 1 -3 1 -1 -1 3 -1 0 2 -2 0 -1 0 6 18 1 -31 5 1 -1 2 1 -1 2 0 1 1 0 2 -1 -136 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0";

# Chuyển đổi chuỗi sang mảng số nguyên
data1 = list(map(float, str1.split()))
data2 = list(map(float, str2.split()))

# Tạo một đồ thị
plt.figure()

# Vẽ đồ thị
plt.plot(data1)
plt.plot(data2)

# Đặt tiêu đề cho đồ thị
plt.title('My Data')

# Hiển thị đồ thị
plt.show()

# ----------------------------------------------------------------------------
# Số lượng phần tử trong file
# with open('graph/1_comma.csv', 'r') as file:
#     data = file.read()
#     elements = data.split(",")
#     num_elements = len(elements)

# print(f'The file contains {num_elements} elements.')

# ----------------------------------------------------------------------------
# Đọc dữ liệu từ file và ghi vào file khác
# Mở file và đọc nội dung
# with open('graph/1.csv', 'r') as file:
#     data = file.read()

# # Thay thế khoảng trắng bằng dấu phẩy
# data = data.replace(' ', ',')

# # Ghi nội dung đã thay đổi vào file mới
# with open('graph/1_comma.csv', 'w') as file:
#     file.write(data)